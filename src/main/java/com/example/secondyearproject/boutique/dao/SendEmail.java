package com.example.secondyearproject.boutique.dao;

/**
 * Created by mohan on 22/1/18.
 */
public class SendEmail {
    //private CustomerEmailOrder customerEmailOrder;
    private String customer_email;
    private String seller_email;

    public SendEmail() {
    }

    public String getCustomer_email() {
        return customer_email;
    }

    public void setCustomer_email(String customer_email) {
        this.customer_email = customer_email;
    }

    //    public CustomerEmailOrder getCustomerEmailOrder() {
//        return customerEmailOrder;
//    }
//
//    public void setCustomerEmailOrder(CustomerEmailOrder customerEmailOrder) {
//        this.customerEmailOrder = customerEmailOrder;
//    }

    public String getSeller_email() {
        return seller_email;
    }

    public void setSeller_email(String seller_email) {
        this.seller_email = seller_email;
    }
}
