package com.example.secondyearproject.boutique.controller;


import com.example.secondyearproject.boutique.dao.SendEmail;
import com.example.secondyearproject.boutique.dto.ActionStatus;
import com.example.secondyearproject.boutique.dto.SendEmailResponseDTO;
import com.example.secondyearproject.boutique.repository.EmailRespository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * Created by mohan on 21/1/18.
 */

@RestController
@RequestMapping("/emailservice")
public class EmailController {


    @Autowired
    EmailRespository emailRespository;



    @GetMapping("/")
    public String healthCheck()
    {
        return "Email Node is working";
    }

    @CrossOrigin
    @PostMapping("/sendconfirmationmail")
    public ActionStatus sendOrderConfirmationMail(@Valid @RequestBody SendEmail sendEmail)
    {
        ActionStatus actionStatus=new ActionStatus();
        actionStatus.setAction_performed("sendconfirmationmail");

        SendEmailResponseDTO customer_conf = emailRespository.sendOrderConfirmationMailToCustomer(sendEmail.getCustomer_email());
        SendEmailResponseDTO seller_conf = emailRespository.sendOrderConfirmationMailToSeller(sendEmail.getSeller_email());

        if (customer_conf.isEmail_sent() && seller_conf.isEmail_sent() )
        {
            customer_conf.setResponse("Email sent to customer successfully");
            seller_conf.setResponse("Email sent to Seller successfully");
            actionStatus.setAction_status("True");
        }
        else {
            actionStatus.setAction_status("False");
        }
        Object []resp={customer_conf,seller_conf};
        actionStatus.setData(resp);


        return actionStatus;
    }

}
