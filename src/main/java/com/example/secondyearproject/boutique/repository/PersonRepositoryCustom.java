package com.example.secondyearproject.boutique.repository;

import com.example.secondyearproject.boutique.dao.Person;

import java.util.List;

public interface PersonRepositoryCustom {
    List<Person> getPersonBy(String name);
}
