package com.example.secondyearproject.boutique.repository;

import com.example.secondyearproject.boutique.dao.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface PersonRepository extends JpaRepository<Person,String> ,PersonRepositoryCustom {

    //@Query("select p FROM Person p where p.name like CONCAT('%',:name,'%')")
    //List<Person> getPersonBy(@Param("name") String name);

}
