package com.example.secondyearproject.boutique.dto;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

/**
 * Created by mohan on 22/1/18.
 */
@Component
public class SendEmailResponseDTO {

    private String response;
    private boolean email_sent;

    public SendEmailResponseDTO()
    {

    }

    public boolean isEmail_sent() {
        return email_sent;
    }

    public void setEmail_sent(boolean email_sent) {
        this.email_sent = email_sent;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }
}
