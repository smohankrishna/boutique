package com.example.secondyearproject.boutique.repository;

import com.example.secondyearproject.boutique.dao.Person;
import org.springframework.transaction.annotation.Transactional;


import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Transactional
public class PersonRepositoryImpl implements PersonRepositoryCustom {

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<Person> getPersonBy(String name) {

        System.out.println("In impll ");
        Query query = em.createNativeQuery("SELECT * FROM Person p " +
                "WHERE p.name LIKE ?", Person.class);
        query.setParameter(1, "%"+name + "%");
        //String query = "SELECT p.* FROM PERSON WHERE NAME LIKE %"+name+"%";

        return query.getResultList();
    }
}
