package com.example.secondyearproject.boutique.repository;

import com.example.secondyearproject.boutique.dao.UserDetails;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<UserDetails,String>,UserRepositoryCustom {
}
