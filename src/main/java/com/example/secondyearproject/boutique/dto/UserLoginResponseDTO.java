package com.example.secondyearproject.boutique.dto;

import com.example.secondyearproject.boutique.dao.Order;

import java.util.List;

public class UserLoginResponseDTO {

    private String email;
    private List<Order> orderList;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<Order> getOrderList() {
        return orderList;
    }

    public void setOrderList(List<Order> orderList) {
        this.orderList = orderList;
    }
}
