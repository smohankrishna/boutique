package com.example.secondyearproject.boutique.repository;

import com.example.secondyearproject.boutique.dao.UserDetails;
import com.example.secondyearproject.boutique.dao.UserLogin;

public interface UserRepositoryCustom {

    boolean loginWithEmailAndPassword(UserLogin userLogin);
    boolean checkRegisterUserExists(UserDetails userDetails);

}
