package com.example.secondyearproject.boutique.repository;


import com.example.secondyearproject.boutique.dao.CustomerEmailOrder;
import com.example.secondyearproject.boutique.dto.SendEmailResponseDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

/**
 * Created by mohan on 21/1/18.
 */

@Component
public class EmailRespository {

    @Autowired
    JavaMailSender javaMailSender;
    @Autowired
    SendEmailResponseDTO sendEmailResponseDTO;





    public SendEmailResponseDTO sendOrderConfirmationMailToCustomer(String customerEmail) {




        String message="Your Order is Confirmed. \n Your order will be deliverd with in 5 Days.\n ";

        String subject = "Order Confirmtion Mail";

        return sendEmail(customerEmail,message,subject);
    }

    public SendEmailResponseDTO sendOrderConfirmationMailToSeller(String seller_email) {

        String message="You got an order of the following Items ";
        String subject = "New Order Arrived";
        return sendEmail(seller_email,message,subject);
    }

    private SendEmailResponseDTO sendEmail(String toEmail, String message_body,String subject) {

        MimeMessage message = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);
        try {
            //adding to
            helper.addTo(toEmail);
            helper.setSubject(subject);
            helper.setText(message_body);

            javaMailSender.send(message);
            sendEmailResponseDTO.setEmail_sent(true);
            sendEmailResponseDTO.setResponse("Email sent successfully");
        } catch (MessagingException e) {
            sendEmailResponseDTO.setResponse("Exception in sending email :"+e.getLocalizedMessage());
            e.printStackTrace();
        }

        return sendEmailResponseDTO;
    }


}
