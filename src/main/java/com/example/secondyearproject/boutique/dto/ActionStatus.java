package com.example.secondyearproject.boutique.dto;

import org.springframework.context.annotation.Bean;

public class ActionStatus {
    private String action_performed;
    private String action_status;
    private Object data;

    public ActionStatus(){}

    public ActionStatus(String action_performed, String action_staus, Object data) {
        this.action_performed = action_performed;
        this.action_status = action_staus;
        this.data = data;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public String getAction_performed() {
        return action_performed;
    }

    public void setAction_performed(String action_performed) {
        this.action_performed = action_performed;
    }

    public String getAction_status() {
        return action_status;
    }

    public void setAction_status(String action_status) {
        this.action_status = action_status;
    }


}
