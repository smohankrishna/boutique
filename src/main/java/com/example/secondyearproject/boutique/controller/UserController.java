package com.example.secondyearproject.boutique.controller;

import com.example.secondyearproject.boutique.dao.UserDetails;
import com.example.secondyearproject.boutique.dao.UserLogin;
import com.example.secondyearproject.boutique.dto.ActionStatus;
import com.example.secondyearproject.boutique.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    public UserRepository userRepository;

    ActionStatus actionStatus;

    @RequestMapping("/")
    public String getUserServerHelath()
    {
        return "user node is working";
    }

    @PostMapping("/registeruser")
    public ActionStatus savePerson(@Valid @RequestBody UserDetails userDetails)
    {
        actionStatus=new ActionStatus();
        actionStatus.setAction_performed("registeruser");
        try {

            if(!userRepository.checkRegisterUserExists(userDetails)) {
                UserDetails userDetails1 = userRepository.save(userDetails);
                actionStatus.setAction_status("true");
                actionStatus.setData(userDetails1);
                return actionStatus;
            }
            actionStatus.setData("User exists with given data");
            actionStatus.setAction_status("false");
            return actionStatus;


//            return userDetails1.getEmail().equals(userDetails.getName()) ? "Saved Succesfully" : "Error in saving";
        }catch (Exception e)
        {
            actionStatus.setAction_status("Exception in Saving User Details exception  : "+e.getMessage());
            actionStatus.setData(userDetails);
            actionStatus.setAction_status("false");
            System.out.println("Exception in Saving User Details exception ");
             e.printStackTrace();
        }
        return actionStatus;
    }

    @PostMapping("/userlogin")
    public ActionStatus userlogin(@Valid @RequestBody UserLogin userLogin)
    {
        actionStatus=new ActionStatus();
        actionStatus.setAction_performed("userlogin");
        try
        {
            boolean loged_in=userRepository.loginWithEmailAndPassword(userLogin);
            actionStatus.setAction_status(String.valueOf(loged_in));
            actionStatus.setData(userLogin.getEmail());

        }catch (Exception e)
        {
            System.out.println("Exception in Login with User Details exception ");
            e.printStackTrace();
            actionStatus.setAction_status("Exception in Login with User Details");
        }
        return actionStatus;
    }

}
