package com.example.secondyearproject.boutique.controller;

import com.example.secondyearproject.boutique.dao.Product;
import com.example.secondyearproject.boutique.dto.ActionStatus;
import com.example.secondyearproject.boutique.dto.ProductAsperCategory;
import com.example.secondyearproject.boutique.repository.ProductRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/product")
public class ProductController {

    @Autowired
    ProductRepository productRepository;

    @Autowired
    ModelMapper modelMapper;
    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }

    @RequestMapping("/")
    public String productNodeCheck()
    {
        return "Product Node is working";
    }


    @PostMapping("/addproduct")
    public ActionStatus addProduct(@Valid @RequestBody Product product){

        System.out.println("Product "+product.getCategory());
        ActionStatus actionStatus=new ActionStatus();
        actionStatus.setAction_performed("add product");
        try {
            if(product.getName()!=null) {
                Product product1 = productRepository.save(product);

                if (product.equals(product1)) {
                    actionStatus.setAction_status("true");
                    actionStatus.setData("Product information saved sucessfully");
                } else {
                    actionStatus.setAction_status("false");
                    actionStatus.setData("Product information not saved");
                }
            }
            else {
                actionStatus.setAction_status("false");
                actionStatus.setData("Product information not saved");
            }
        }catch (Exception e) {
            System.out.println("Exception product saving");
            e.printStackTrace();
            actionStatus.setAction_status("false");
            actionStatus.setData("Exception in product data saving..");
        }
        return actionStatus;
    }


    @GetMapping("/getallproducts")
    public ActionStatus getAllproducts()
    {
        ActionStatus actionStatus=new ActionStatus();
        actionStatus.setAction_performed("get all product");
        try {
            actionStatus.setAction_status("true");
            actionStatus.setData(productRepository.findAll());
        }catch (Exception e) {
            System.out.println("Exception in getting all products");
            e.printStackTrace();
            actionStatus.setAction_status("false");
            actionStatus.setData("Exception in getting all products..");
        }
        return actionStatus;
    }

    @GetMapping("/getAllCategories")
    public ActionStatus getAllCategories()
    {
        ActionStatus actionStatus=new ActionStatus();
        actionStatus.setAction_performed("getAllCategories ");
        try {
            actionStatus.setData(productRepository.findAllCategories());
            actionStatus.setAction_status("true");
        }catch (Exception e)
        {
            System.out.println("Exception in getting all categories ");
            e.printStackTrace();
            actionStatus.setData("Exception in getting all categories ");
            actionStatus.setAction_status("false");
        }

        return actionStatus;
    }

    @PutMapping("/updateproduct")
    public ActionStatus updateProductDetails(@Valid @RequestBody Product product)
    {
        ActionStatus actionStatus=new ActionStatus();
        actionStatus.setAction_performed("Update Product");
        try
        {

            actionStatus.setData(productRepository.save(product));
            actionStatus.setAction_status("true");
        }catch (Exception e)
        {
            System.out.println("Exception in Updating the product");
            e.printStackTrace();
            actionStatus.setAction_status("false");
            actionStatus.setData("Exception in Updating the product..");
        }

        return actionStatus;
    }


    @DeleteMapping("/deleteproduct")
    public ActionStatus deleteproductDetails(@Valid @RequestBody Product product)
    {
        ActionStatus actionStatus=new ActionStatus();
        actionStatus.setAction_performed("Delete Product");
        try
        {
            productRepository.delete(product);
            actionStatus.setAction_status("true");
        }catch (Exception e)
        {
            System.out.println("Exception in deleting the product");
            e.printStackTrace();
            actionStatus.setAction_status("false");
            actionStatus.setData("Exception in deleting the product..");
        }

        return actionStatus;
    }

    @GetMapping("/getProductsofgivenCategory")
    public ActionStatus getProductsofgivenCategory(@RequestParam("category") String caterogry)
    {

        ActionStatus actionStatus=new ActionStatus();
        actionStatus.setAction_performed("get Products of given Category");
        try
        {

            actionStatus.setData((productRepository.findByCategory(caterogry)));
            actionStatus.setAction_status("true");
        }catch (Exception e)
        {
            System.out.println("Exception in getting products from given category");
            e.printStackTrace();
            actionStatus.setAction_status("false");
            actionStatus.setData("Exception in getting products from given category..");
        }
        return actionStatus;
    }

    private List<ProductAsperCategory> maptoProduct(List<Product> productList) {
        List<ProductAsperCategory> mapped_proProductList1=new ArrayList<>();
        for (int i=0;i<productList.size();i++) {

            System.out.println(productList.get(i).getName());


            mapped_proProductList1.add(modelMapper.map(productList.get(i),ProductAsperCategory.class));
        }

        return mapped_proProductList1;
    }

}
