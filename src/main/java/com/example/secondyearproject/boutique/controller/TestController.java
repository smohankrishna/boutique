package com.example.secondyearproject.boutique.controller;

import com.example.secondyearproject.boutique.dao.Person;
import com.example.secondyearproject.boutique.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class TestController {

    @Autowired
    PersonRepository personRepository;

//    @Autowired
//    PersonRepositoryImpl customJPARepositoryImpl;



    @RequestMapping("/name")
    public String getName()
    {
        return "Test Person is Working";
    }


    @GetMapping(value = "/getallnames")
    public List getAllNames()
    {
        return personRepository.findAll();
    }

    @GetMapping("/getpersondata/{name}")
    public Person findOrderById(@PathVariable String name) {


        return personRepository.findOne(name);
    }

    @GetMapping("/getmatchedpersonswithname/{name}")
    public List<Person> getmatchedpersonswithname(@PathVariable String name)
    {
        System.out.println("name:- "+name);
        List<Person> p= personRepository.getPersonBy(name);
        if(p==null)
            System.out.println("no persons found");
        return p;
    }

    @PostMapping("/createperson")
    public String savePerson(@Valid @RequestBody Person person)
    {
        Person p= personRepository.save(person);
        return person.getName().equals(p.getName())?"Saved Succesfully":"Error in saving";


    }

}
