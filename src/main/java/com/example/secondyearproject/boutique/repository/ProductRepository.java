package com.example.secondyearproject.boutique.repository;

import com.example.secondyearproject.boutique.dao.Product;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.Repository;

import java.util.List;

public interface ProductRepository extends CrudRepository<Product,Integer>, Repository<Product,Integer>{

    @Query("SELECT p FROM Product p where p.category = ?1")
    List<Product> findByCategory(String cateogry);

    @Query("SELECT distinct category FROM Product p")
    List<Product> findAllCategories();
}
