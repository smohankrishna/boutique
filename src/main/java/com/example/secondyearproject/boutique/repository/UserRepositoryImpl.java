package com.example.secondyearproject.boutique.repository;

import com.example.secondyearproject.boutique.dao.UserDetails;
import com.example.secondyearproject.boutique.dao.UserLogin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public class UserRepositoryImpl implements UserRepositoryCustom {

    @Autowired
    public UserRepository userRepository;

    @Override
    public boolean loginWithEmailAndPassword(UserLogin userLogin) {
        UserDetails userDetails=userRepository.findOne(userLogin.getEmail());
        System.out.println(userDetails!=null?"login userdetails found :- "+userDetails.getEmail():"login userdetails not found");
//        if(userDetails!=null && userDetails.getEmail().equals(userLogin.getEmail()) && userDetails.getEmail().equals(userLogin.getPassword()))
//            return true;

       return userDetails!=null && userDetails.getEmail().equals(userLogin.getEmail()) && userDetails.getPassword().equals(userLogin.getPassword());
    }

    @Override
    public boolean checkRegisterUserExists(UserDetails userDetails) {
        UserDetails usercheck=userRepository.findOne(userDetails.getEmail());

        if(usercheck!=null && usercheck.getMobile().equals(userDetails.getMobile())) {
            return true;
        }
        return false;
    }
}
