package com.example.secondyearproject.boutique.dao;

import java.util.List;

/**
 * Created by mohan on 21/1/18.
 */
public class CustomerEmailOrder {

    private String customer_email;
    //private List<Product> productList;

    public CustomerEmailOrder()
    {

    }

    public String getCustomer_email() {
        return customer_email;
    }

    public void setCustomer_email(String customer_email) {
        this.customer_email = customer_email;
    }

//    public List<Product> getProductList() {
//        return productList;
//    }
//
//    public void setProductList(List<Product> productList) {
//        this.productList = productList;
//    }
}
